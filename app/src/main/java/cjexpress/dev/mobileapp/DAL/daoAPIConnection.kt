package cjexpress.dev.mobileapp.DAL

import android.os.Build
import android.os.StrictMode
import org.json.JSONException
import org.json.JSONObject
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.InputStreamReader
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import javax.net.ssl.HttpsURLConnection

class daoAPIConnection {
    val Endpoint = "http://192.168.1.63:45455/MobileAppService.svc/"
    val GET = 1
    val POST =2

    fun ExecuteRequest(
        ptFunction: String,
        pnMethod: Int,
        poJSONObject: JSONObject?
    ): String? {
        var oHttpConnection: HttpURLConnection? = null
        var oUrl: URL? = null
        var tResult: String? = null
        var tParams: String? = ""
        return try {
            //region Permission StrictMode
            if (Build.VERSION.SDK_INT > 9) {
                val policy =
                    StrictMode.ThreadPolicy.Builder().permitAll().build()
                StrictMode.setThreadPolicy(policy)
            }
            when (pnMethod) {
                GET -> {
                    //region Parameters
                    if (poJSONObject != null && poJSONObject.length() > 0) {
                        var nI = 0
                        while (nI <= poJSONObject.length() - 1) {
                            try {
                                tParams += "/" + poJSONObject[poJSONObject.names().getString(nI)]
                            } catch (ex: JSONException) {
//                                spoDBUtil.oException = ex
                                return null
                            }
                            nI++
                        }
                    }
                    //endregion
                    oUrl =
                        URL(Endpoint + ptFunction + tParams)
                    oHttpConnection = oUrl.openConnection() as HttpURLConnection
                    oHttpConnection.requestMethod = "GET"
                    oHttpConnection!!.readTimeout = 5000
                }
                POST -> {
                    oUrl = URL(Endpoint + ptFunction)
                    oHttpConnection = oUrl.openConnection() as HttpURLConnection
                    oHttpConnection!!.doOutput = true
                    oHttpConnection.requestMethod = "POST"
                    oHttpConnection.setRequestProperty(
                        "Content-Type",
                        "application/json;charset=utf-8"
                    )
                    oHttpConnection.readTimeout = 5000
                    if (poJSONObject != null) {
                        val outputStream = oHttpConnection.outputStream
                        val bufferedWriter = BufferedWriter(
                            OutputStreamWriter(
                                outputStream,
                                "UTF-8"
                            )
                        )
                        bufferedWriter.write(poJSONObject.toString())
                        bufferedWriter.flush()
                        bufferedWriter.close()
                        outputStream.close()
                    }
                }
            }
            oHttpConnection!!.connectTimeout = 5000
            if (oHttpConnection.responseCode == HttpsURLConnection.HTTP_OK) {
                val `in` =
                    BufferedReader(InputStreamReader(oHttpConnection.inputStream))
                var tInputLine: String?
                val oResponse = StringBuffer()
                while (`in`.readLine().also { tInputLine = it } != null) {
                    oResponse.append(tInputLine)
                }
                `in`.close()
                tResult = oResponse.toString()
            }
            tResult
        } catch (ex: Exception) {
            ex.message
        } finally {
            oHttpConnection!!.disconnect()
            oHttpConnection = null
            oUrl = null
            tResult = null
            tParams = null
        }
    }
}