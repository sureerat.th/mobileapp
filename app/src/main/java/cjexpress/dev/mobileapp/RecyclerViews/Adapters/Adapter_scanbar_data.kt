package cjexpress.dev.mobileapp.RecyclerViews.Adapters

import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.RecyclerView
import cjexpress.dev.mobileapp.DML.mdoTDocTBoDT
import cjexpress.dev.mobileapp.R
import cjexpress.dev.mobileapp.RecyclerViews.ViewHolders.ViewHolder_scanbar_data
import kotlinx.android.synthetic.main.dialog_scanbar_dataedit.view.*
import kotlinx.android.synthetic.main.item_scanbar_data.view.*

class Adapter_scanbar_data(val poContext: Context,var poData: ArrayList<mdoTDocTBoDT>?) : RecyclerView.Adapter<ViewHolder_scanbar_data>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder_scanbar_data {
        val oView = LayoutInflater.from(poContext).inflate(R.layout.item_scanbar_data,parent,false)
        return ViewHolder_scanbar_data(oView)
    }

    override fun getItemCount(): Int {
        if (poData != null) {
            return poData!!.size
        }else{
            return 0
        }
    }

    override fun onBindViewHolder(holder: ViewHolder_scanbar_data, position: Int) {
        if (poData != null){
            holder.itemView.otvw_itemscanbar_barcode.text = poData!![position].XVBarCode
            holder.itemView.otvw_itemscanbar_qty.text = poData!![position].XFTsdQty.toString()

            holder.itemView.setOnClickListener {
                //หน้าตาของdialog
                val oDialogView = LayoutInflater.from(poContext).inflate(R.layout.dialog_scanbar_dataedit,null)
                //ตัวBuild
                val oBuilder = AlertDialog.Builder(poContext)
                //ตัวoBuilder ไปเอาหน้าตาของdialog_scanbar_dataeditหน้านี้ ก็คือoDialogViewนะ
                oBuilder.setView(oDialogView)
                //ตัวBuilderที่โชว์ขึ้นมา เก็บไว้ที่oDialog
                val oDialog = oBuilder.show()

                oDialogView.oetx_dialogscanbar_qty.setText(poData!![position].XFTsdQty.toString())

                oDialogView.oimb_dialogscanbar_minus.setOnClickListener{
                    if (oDialogView.oetx_dialogscanbar_qty.text.toString().toDouble().minus(1.0) >= 0){
                        oDialogView.oetx_dialogscanbar_qty.setText(oDialogView.oetx_dialogscanbar_qty.text.toString().toDouble().minus(1.0).toString())
                    }else{
                        val oDialog = AlertDialog.Builder(poContext)
                        oDialog.setTitle("แจ้งเตือน")
                        oDialog.setIcon(R.drawable.iconfinder_alert)
                        oDialog.setMessage("จำนวนต้องไม่น้อยกว่า 0")
                        oDialog.setCancelable(false)
                        oDialog.setPositiveButton("ตกลง", DialogInterface.OnClickListener { dialogInterface, i -> dialogInterface.dismiss() })
                        oDialog.show()
                    }
                }

                oDialogView.oimb_dialogscanbar_plus.setOnClickListener{
                    oDialogView.oetx_dialogscanbar_qty.setText(oDialogView.oetx_dialogscanbar_qty.text.toString().toDouble().plus(1.0).toString())
                }

                oDialogView.obtn_dialogscanbar_save.setOnClickListener {
                    var oDT = poData!![position]
                    poData!!.remove(oDT)

                    oDT.XFTsdQty = oDialogView.oetx_dialogscanbar_qty.text.toString().toDouble()
                    poData!!.add(0,oDT)

                    notifyDataSetChanged()
                    oDialog.dismiss()
                }

                oDialogView.obtn_dialogscanbar_delete.setOnClickListener {
                    var oDT = poData!![position]
                    poData!!.remove(oDT)

                    notifyDataSetChanged()
                    oDialog.dismiss()
                }

            }
        }
    }
}