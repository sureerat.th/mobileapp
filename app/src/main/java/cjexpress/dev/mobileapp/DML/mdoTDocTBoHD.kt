package cjexpress.dev.mobileapp.DML

import java.util.*
import kotlin.collections.ArrayList

data class mdoTDocTBoHD(
    var XVTrhDocNo: String?,
    var XVTrhDocDate: Date?,
    var ListDT: ArrayList<mdoTDocTBoDT>
)
{
    constructor():this(null,null,ArrayList())
}