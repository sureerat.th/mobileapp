package cjexpress.dev.mobileapp.DML

data class mdoTDocTBoDT (
    var XVTrhDocNo: String?,
    var XVSkuCode: String?,
    var XVBarCode: String?,
    var XFTsdQty: Double?
)
{
    constructor() : this(null,null,null,null)
}

