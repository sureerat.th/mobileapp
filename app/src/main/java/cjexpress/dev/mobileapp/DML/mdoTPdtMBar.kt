package cjexpress.dev.mobileapp.DML

data class mdoTPdtMBar (
    var XVSukCode: String?,
    var XVSukName: String?,
    var XVBarCode: String?
)
{
    constructor() : this(null,null,null)
}
