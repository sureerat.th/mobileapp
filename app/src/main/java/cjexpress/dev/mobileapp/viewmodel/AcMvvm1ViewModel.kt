package cjexpress.dev.mobileapp.viewmodel

import android.app.Application
import android.content.Intent
import android.view.View
import androidx.core.content.ContextCompat.startActivity
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import cjexpress.dev.mobileapp.ac_mvvm_1
import cjexpress.dev.mobileapp.ac_scanbar
import cjexpress.dev.mobileapp.model.UserModel

class AcMvvm1ViewModel(app: Application) : AndroidViewModel(app) {

    var user = MutableLiveData<String>()
    var pass = MutableLiveData<String>()
    var msg = MutableLiveData<String>()

    var number = MutableLiveData<String>("0")
    var isShow = MutableLiveData(View.VISIBLE)

    fun login() {
        if(user.value.isNullOrBlank() || pass.value.isNullOrBlank()) {
            return
        } else {
            var oModel1 = UserModel("009","009")
            var oModel2 = UserModel("009","009")

            if(oModel1 == oModel2){
                msg.value = "TRUE"
            } else {
                msg.value = "FALSE"
            }
        }
    }

    fun setPlusNumber() {
        number.value = number.value!!.toInt().plus(1).toString()

        if (number.value!!.toInt() % 2 == 0) {
            isShow.value = View.INVISIBLE
        } else {
            isShow.value = View.VISIBLE
        }
    }
}
