package cjexpress.dev.mobileapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.widget.Adapter
import android.widget.LinearLayout
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import cjexpress.dev.mobileapp.DML.mdoTDocTBoDT
import cjexpress.dev.mobileapp.DML.mdoTDocTBoHD
import cjexpress.dev.mobileapp.DML.mdoTPdtMBar
import cjexpress.dev.mobileapp.RecyclerViews.Adapters.Adapter_scanbar_data
import kotlinx.android.synthetic.main.activity_scanbar.*
import kotlin.math.log

class ac_scanbar : AppCompatActivity() {

    private var goDataHD: mdoTDocTBoHD = mdoTDocTBoHD()
    private var goAdapter: Adapter_scanbar_data? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanbar)

        otvw_scanbar_name.text = otvw_scanbar_name.text.toString() +" "+ intent.getStringExtra("Username")

        oetx_scanbar_barcode.setOnKeyListener {view, i, keyEvent ->
            if (i == KeyEvent.KEYCODE_ENTER && keyEvent.action == KeyEvent.ACTION_UP){
                obtn_scanbar_add.performClick()
                return@setOnKeyListener true
            }
            false
        }

        obtn_scanbar_add.setOnClickListener {
            if(otvw_scanbar_name.text.toString().isNullOrBlank()){
                otvw_scanbar_name.setError("กรุรากรอกบาร์โค้ด")
                return@setOnClickListener
            }

            var oTemp = mdoTDocTBoDT()
            oTemp.XVBarCode = oetx_scanbar_barcode.text.toString()
            oTemp.XFTsdQty = 1.0

            //ข้างบนเราให้ค่าเป็น goDataHD = mdoTDocTBoHD() แล้ว ก็ไม่ต้องเช็คnull
//            if (goDataHD == null){
//                goDataHD = mdoTDocTBoHD()
//            }

            if (goDataHD.ListDT == null){
                goDataHD.ListDT = ArrayList()
            }

            var oDT :mdoTDocTBoDT? = goDataHD.ListDT!!.find { it -> it.XVBarCode == oetx_scanbar_barcode.text.toString()}
            if (oDT != null){
                goDataHD.ListDT!!.remove(oDT)
                oDT.XFTsdQty = oDT.XFTsdQty?.plus(1)
                goDataHD.ListDT!!.add(0,oDT)
            }else{
                goDataHD.ListDT?.add(0,oTemp)
            }

            orcv_scanbar_data.adapter!!.notifyDataSetChanged()

            Log.d("kfc",goDataHD.ListDT.toString())
            oetx_scanbar_barcode.text.clear()
        }

        goAdapter = Adapter_scanbar_data(this,goDataHD.ListDT)
        orcv_scanbar_data.layoutManager = LinearLayoutManager(this)
        orcv_scanbar_data.addItemDecoration(DividerItemDecoration(this,LinearLayoutManager.VERTICAL))
        orcv_scanbar_data.adapter = goAdapter



    }
}
