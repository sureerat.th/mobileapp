package cjexpress.dev.mobileapp

import android.content.DialogInterface
import android.content.Intent
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import cjexpress.dev.mobileapp.BLL.bloLogin
import cjexpress.dev.mobileapp.DML.mdoTUsrMUser
import kotlinx.android.synthetic.main.activity_login.*

class ac_login : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        obtn_login_login.setOnClickListener{
            if(oetx_login_username.text.isNullOrBlank()){
                oetx_login_username.setError("กรณากรอก Username")
                return@setOnClickListener
            }

            if(oetx_login_password.text.isNullOrBlank()){
                oetx_login_password.setError("กรุณากรอก Password")
                return@setOnClickListener
            }


            var oTemp = mdoTUsrMUser()
            oTemp.XVUsrCode = oetx_login_username.text.toString()
            getUser(oTemp).execute()

            if(oetx_login_username.text.toString() == "009" && oetx_login_password.text.toString() == "009"){
                val oIntent = Intent(this,ac_scanbar::class.java)
                oIntent.putExtra("Username",oetx_login_username.text.toString())
                startActivity(oIntent)
                finish()

            }else {
                val oDialog = AlertDialog.Builder(this)
                oDialog.setTitle("แจ้งเตือน")
                oDialog.setIcon(R.drawable.iconfinder_alert)
                oDialog.setMessage("Username หรือ Password ไม่ถูกต้อง")
                oDialog.setCancelable(false)
                oDialog.setPositiveButton("ตกลง", DialogInterface.OnClickListener { dialogInterface, i -> dialogInterface.dismiss() })
                oDialog.show()
            }
        }

        obtn_login_testservice.setOnClickListener {
            getWebServiceStatus().execute()

        }
    }

    inner class getWebServiceStatus() : AsyncTask<Void,Void, String?>(){
        override fun doInBackground(vararg params: Void?): String? {
            val oBll = bloLogin()
            val tValue = oBll.getWebServiceStatus()
            return tValue
        }

        override fun onPostExecute(result: String?) {
            val oDialog = AlertDialog.Builder(this@ac_login)
            oDialog.setTitle("แจ้งเตือน")
            //oDialog.setIcon(R.drawable.iconfinder_alert)
            oDialog.setMessage(result)
            //oDialog.setCancelable(false)
            oDialog.setPositiveButton("ตกลง", DialogInterface.OnClickListener { dialogInterface, i -> dialogInterface.dismiss() })
            oDialog.show()
        }
    }

    class getUser(var poData: mdoTUsrMUser) : AsyncTask<Void,Void, mdoTUsrMUser?>(){
        override fun doInBackground(vararg params: Void?): mdoTUsrMUser? {
            val oBll = bloLogin()
            return oBll.getUser(poData)
        }
    }
}
