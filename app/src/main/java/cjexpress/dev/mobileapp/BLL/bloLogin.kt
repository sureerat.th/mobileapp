package cjexpress.dev.mobileapp.BLL

import cjexpress.dev.mobileapp.DAL.daoAPIConnection
import cjexpress.dev.mobileapp.DAL.daoTUsrMUser
import cjexpress.dev.mobileapp.DML.mdoTUsrMUser
import org.json.JSONObject

class bloLogin {

    fun getWebServiceStatus():String?{
        val daoAPI = daoAPIConnection()
        val tResult = daoAPI.ExecuteRequest("getWebServiceStatus",daoAPI.GET,null)

        if (tResult.isNullOrBlank()){
            return "ไม่สามารถเชื่อมต่อกับเว็บเซอร์วิสได้"
        }else{
            val oJson = JSONObject(tResult)
            val tValue = oJson.get("getWebServiceStatusResult").toString()

            if (tValue.toUpperCase() == "ONLINE"){
                return "เชื่อมต่อสำเร็จ"
            }else{
                return "เว็บเซอร์วิสอยู่ในสถานะไม่พร้อมทำงาน"
            }
        }


    }

    fun getUser(poData: mdoTUsrMUser): mdoTUsrMUser? {
        val daoData = daoTUsrMUser()
        val  tResult = daoData.getUser(poData)

        return null
    }
}