package cjexpress.dev.mobileapp

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import cjexpress.dev.mobileapp.databinding.AcMvvm1Binding
import cjexpress.dev.mobileapp.viewmodel.AcMvvm1ViewModel
import kotlinx.android.synthetic.main.ac_mvvm_1.*

class ac_mvvm_1 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val viewmodel by viewModels <AcMvvm1ViewModel>()
        val binding: AcMvvm1Binding = DataBindingUtil.setContentView(this, R.layout.ac_mvvm_1)
        binding.viewmodel = viewmodel
        binding.lifecycleOwner = this

        viewmodel.number.observe(this, Observer {
            if (it.toInt() % 2 == 0) {
                button2.setBackgroundColor(Color.RED)
            } else {
                button2.setBackgroundColor(Color.GREEN)
            }
        })

        viewmodel.isShow.observe(this, Observer {
            if(it == View.VISIBLE) {
                button.visibility = View.VISIBLE
            } else {
                button.visibility = View.INVISIBLE
            }
        })

    }
}
